# TODOs

## Bug fixes

- ~~Improve content block draggable target areas~~
- ~~Remove extra spacing for single line heading~~

## Feature enhancements

- More robust content block inserting
- Ability to **bold** body text
- Media inserting/positioning

# Stack

- [Angular JS](http://angularjs.org/)
- [Angular UI](http://angular-ui.github.io/)
- [jQuery UI](http://jqueryui.com/)

## Plugins & scripts

- [Autosize](http://www.jacklmoore.com/autosize/)
- [Expanding text areas](http://alistapart.com/article/expanding-text-areas-made-elegant)

# Other

- Edit-in-place
- Drag-and-drop