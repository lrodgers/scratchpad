var myAppModule = angular.module('MyApp', ['ui.sortable', 'ngCookies'])

myAppModule.controller('ContentController', function($scope, $cookies) {


  // $scope.title; /*"How to Prepare Your Mac for Mountain Lion"*/
  // $scope.items = [
  //  {heading: "Before You Upgrade, Part 1: Find Out If Your Mac and Apps Are Compatible with Mountain Lion", subheading: "", body: "Nullam quis risus eget urna mollis ornare vel eu leo. Vivamus sagittis lacus vel augue laoreet rutrum faucibus dolor auctor. Etiam porta sem malesuada magna mollis euismod. Nulla vitae elit libero, a pharetra augue. Donec sed odio dui."}
  // ,{heading: "Check if your apps are Mountain Lion-compatible", subheading: "Quam Nibh Fringilla Mollis Adipiscing", body: "Fusce dapibus, tellus ac cursus commodo, tortor mauris condimentum nibh, ut fermentum massa justo sit amet risus. Nulla vitae elit libero, a pharetra augue. Aenean lacinia bibendum nulla sed consectetur. Etiam porta sem malesuada magna mollis euismod. Donec sed odio dui."}
  // ,{heading: "Before You Upgrade, Part 2: Clean Up and Optimize Your Hard Drive", subheading: "Amet Dapibus Quam", body: "Fusce dapibus, tellus ac cursus commodo, tortor mauris condimentum nibh, ut fermentum massa justo sit amet risus. Nulla vitae elit libero, a pharetra augue. Aenean lacinia bibendum nulla sed consectetur. Etiam porta sem malesuada magna mollis euismod. Donec sed odio dui."}
  // ,{heading: "Before You Upgrade, Part 3: Back Up Your Mac", subheading: "Amet Dapibus Quam", body: "Fusce dapibus"}
  // ]


  $scope.sortable = {
    // distance: 20,
    axis: "y",
    helper: "clone",
    revert: 100,
    placeholder: "sortable-placeholder"
  }

  $scope.content = []

  $scope.delete = function(ix) {
    $scope.content.splice(ix, 1)
  }

  $scope.putSection = function(heading, body) {
    $scope.content.push({"heading": heading, "body": body})
    $scope.heading = ''
    $scope.body = ''
  }

  

  // $scope.items = ['one', 'two'];
  // $scope.selected = $scope.items[0];

  $scope.itemClass = function(item) {
      return item === $scope.selected ? 'active' : undefined;
  };
  
  /* 
   * CAN'T GET COOKIES TO LOAD!!!
   */
  // $scope.putCookie = function(key, value) {
  //   $cookies[key] = value
  //   console.log("key: " + key + ", value: " + $cookies[key])
  // }

  // $scope.getCookie = function(value) {
  //   $scope.title = $cookies[value]
  //   console.log($scope.title)
  // }

})